import unittest

from unittest import mock
from satellite_tle import fetch_tles_from_url


class TestFetchTleFromUrlMethod(unittest.TestCase):
    @mock.patch('satellite_tle.fetch_tle.requests')
    def test_tle_file_simple(self, requests):
        requests.get.return_value = mock.MagicMock()
        requests.get.return_value.raise_for_status = mock.Mock()
        requests.get.return_value.text = (
            "BOBCAT-1                \n"
            "1 46922U 98067RS  21110.83931133  .00021294  00000-0  32302-3 0  9990\n"
            "2 46922  51.6423 262.4338 0001752 235.0482 125.0345 15.54552748 25871\n")

        url = "https://www.example.com/mocked/tle_file_fixture.txt"
        tles = fetch_tles_from_url(url=url)
        self.assertEqual(len(tles), 1)

    @mock.patch('satellite_tle.fetch_tle.requests')
    def test_tle_file_with_trailing_newlines(self, requests):
        requests.get.return_value = mock.MagicMock()
        requests.get.return_value.raise_for_status = mock.Mock()
        requests.get.return_value.text = (
            "BOBCAT-1                \n"
            "1 46922U 98067RS  21110.83931133  .00021294  00000-0  32302-3 0  9990\n"
            "2 46922  51.6423 262.4338 0001752 235.0482 125.0345 15.54552748 25871\n\n")

        url = "https://www.example.com/mocked/tle_file_fixture.txt"
        tles = fetch_tles_from_url(url=url)
        self.assertEqual(len(tles), 1)
